<?php

namespace Dse\ElementsBundle\ElementFullwidthimage\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementFullwidthimage;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementFullwidthimage\DseElementFullwidthimage::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
